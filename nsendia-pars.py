import time
import csv
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Firefox()
url = 'https://www.nseindia.com/'

driver.get(url)
# Hover on MARKET DATA
market_data_element = driver.find_element(by=By.CSS_SELECTOR, value='#link_2')
action = ActionChains(driver)
action.move_to_element(market_data_element).perform()
# Find and click in "Pre-Open Market"
pre_open_market_element = WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, "#main_navbar > ul > li:nth-child(3) > div > div.container > div > div:nth-child(1) > ul > li:nth-child(1) > a"))
)
pre_open_market_element.click()
# Load data
WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.CSS_SELECTOR, "#livePreTable > tbody > tr:nth-child(1)"))
)
# All elems with price
# FIXME
price_elements = driver.find_elements(by=By.CSS_SELECTOR, value="#livePreTable > tbody > tr > td:nth-child(9)")
with open('prices.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Имя', 'Цена'])
    # write to CSV
    for price_element in price_elements:
        name_element = price_element.find_element(by=By.CSS_SELECTOR,value="td:nth-child(2)")
        name = name_element.text
        price = price_element.text.replace("#", ' ')
        writer.writerow([name, price])

market_data_element = driver.find_element(by=By.CSS_SELECTOR, value='#link_0')
action.click(market_data_element).perform()
time.sleep(1)
market_data_element = WebDriverWait(driver, 20).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, '#tabList_NIFTYBANK'))
)
market_data_element.click()

view_all_button = driver.find_element(By.CSS_SELECTOR, '#gainers_loosers > div.link-wrap > a')

# Go to "ViewAll"
driver.execute_script("arguments[0].click();", view_all_button)
dd_elem = (WebDriverWait(driver, 20)
           .until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#equitieStockSelect"))))
dd_elem.click()
dropdown = Select(dd_elem)
element_name = 'NIFTY ALPHA 50'
dropdown.select_by_value(element_name)

table = driver.find_element(By.CSS_SELECTOR,'#equityStockTable')
driver.execute_script("arguments[0].scrollTo(0, arguments[0].scrollHeight);", table)