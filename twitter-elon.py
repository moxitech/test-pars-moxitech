import requests
import re
from fake_headers import Headers
from requests_html import HTMLSession

def get_tokens(tweet_url):
    _header = Headers(
        browser="chrome",
        os="win",
        headers=True
    )
    print("Получаю токены")
    session = HTMLSession()
    r = session.get(tweet_url, headers=_header.generate())

    assert r.status_code == 200, f'Не найдена страница!'

    mainjs_url = re.findall(r'https://abs.twimg.com/responsive-web/client-web-legacy/main.[^\.]+.js', r.text)

    mainjs_url = mainjs_url[0]

    mainjs = requests.get(mainjs_url)

    assert mainjs.status_code == 200, f'Ошибка получения main.js'

    bearer_token = re.findall(r'AAAAAAAAA[^"]+', mainjs.text)

    assert bearer_token is not None and len(
        bearer_token) > 0, f'ТОКЕН НЕ НАЙДЕН'

    bearer_token = bearer_token[0]

    with requests.Session() as s:
        s.headers.update({
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0",
            "accept": "*/*",
            "accept-language": "de,en-US;q=0.7,en;q=0.3",
            "accept-encoding": "gzip, deflate, br",
            "te": "trailers", })

        s.headers.update({"authorization": f"Bearer {bearer_token}"})

        guest_token = s.post(
            "https://api.twitter.com/1.1/guest/activate.json").json()["guest_token"]

    assert guest_token is not None, f'Failed to find guest token.  If you are using the correct Twitter URL this suggests a bug in the script.  Please open a GitHub issue and copy and paste this message.  Tweet url: {tweet_url}, main.js url: {mainjs_url}'

    return bearer_token, guest_token

def get_tweets(bearer_token, guest_token ):
    url = "https://api.twitter.com/graphql/LxVacKhIjjbWHu1DxjMvsQ/UserTweets?variables=%7B%22userId%22%3A%2244196397%22%2C%22count%22%3A20%2C%22includePromotedContent%22%3Atrue%2C%22withQuickPromoteEligibilityTweetFields%22%3Atrue%2C%22withVoice%22%3Atrue%2C%22withV2Timeline%22%3Atrue%7D&features=%7B%22rweb_tipjar_consumption_enabled%22%3Afalse%2C%22responsive_web_graphql_exclude_directive_enabled%22%3Atrue%2C%22verified_phone_label_enabled%22%3Afalse%2C%22creator_subscriptions_tweet_preview_api_enabled%22%3Atrue%2C%22responsive_web_graphql_timeline_navigation_enabled%22%3Atrue%2C%22responsive_web_graphql_skip_user_profile_image_extensions_enabled%22%3Afalse%2C%22communities_web_enable_tweet_community_results_fetch%22%3Atrue%2C%22c9s_tweet_anatomy_moderator_badge_enabled%22%3Atrue%2C%22tweetypie_unmention_optimization_enabled%22%3Atrue%2C%22responsive_web_edit_tweet_api_enabled%22%3Atrue%2C%22graphql_is_translatable_rweb_tweet_is_translatable_enabled%22%3Atrue%2C%22view_counts_everywhere_api_enabled%22%3Atrue%2C%22longform_notetweets_consumption_enabled%22%3Atrue%2C%22responsive_web_twitter_article_tweet_consumption_enabled%22%3Atrue%2C%22tweet_awards_web_tipping_enabled%22%3Afalse%2C%22freedom_of_speech_not_reach_fetch_enabled%22%3Atrue%2C%22standardized_nudges_misinfo%22%3Atrue%2C%22tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled%22%3Atrue%2C%22rweb_video_timestamps_enabled%22%3Atrue%2C%22longform_notetweets_rich_text_read_enabled%22%3Atrue%2C%22longform_notetweets_inline_media_enabled%22%3Atrue%2C%22responsive_web_enhance_cards_enabled%22%3Afalse%7D&fieldToggles=%7B%22withArticlePlainText%22%3Afalse%7D"
    print("Получаю твиты")
    payload = {}
    headers = {
        'authority': 'api.twitter.com',
        'accept': '*/*',
        'accept-language': 'ru,en;q=0.9',
        'authorization': f'Bearer {bearer_token}',
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        'cookie': f'guest_id=v1%3A{guest_token}; d_prefs=MToxLGNvbnNlbnRfdmVyc2lvbjoyLHRleHRfdmVyc2lvbjoxMDAw; guest_id_ads=v1%3A{guest_token}; guest_id_marketing=v1%3A{guest_token}; personalization_id="v1_RuTRSRF1X+UjfB5VpLtFKA=="; external_referer=padhuUp37zhU%2Fo1ilYwCNOmtabS4UqDS|0|8e8t2xd8A2w%3D; _ga=GA1.2.1391595356.1711689115; twtr_pixel_opt_in=Y; gt={guest_token}; _gid=GA1.2.1524139132.1712085551; guest_id=v1%3A{guest_token}',
        'origin': 'https://twitter.com',
        'pragma': 'no-cache',
        'referer': 'https://twitter.com/',
        'sec-ch-ua': '"Not_A Brand";v="8", "Chromium";v="120", "YaBrowser";v="24.1", "Yowser";v="2.5"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-site',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 YaBrowser/24.1.0.0 Safari/537.36',
        'x-client-transaction-id': 'eqX7mZ/K+gcbAFKWkdJr+lUKCCrmrALmoYeTclcDr2rK0iTfEFPC1yWWRNunBF5EcQ9wx3vEUsa8X3varaIPszSf0Tv8ew',
        'x-guest-token': '1775241570549158297',
        'x-twitter-active-user': 'yes',
        'x-twitter-client-language': 'ru'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    # print(response.text)
    return response.json()
URL = 'https://twitter.com/elonmusk'
print(f"Открываю страницу {URL}")
print("Если память не изменяет, то это страница Илона Маска! ;)")
b, g = get_tokens(URL)
#print(b, g)
payload = get_tweets(b, g)
print("Компьютер загружает данные...")
with open('tweets.json', "w", encoding="utf-8") as f:
    f.write(str(payload))
print("Теперь-то можно посмотреть что интересного у нашего западного коллеги на странице!")
u = input("На сколько твитов меня хватит...? : ")
if int(u) > 90:
    print("Думаю от такого количества новостей лопнет мозг. Да и... к тому же они будут старыми")
    u = 50
if int(u) < 5:
    print("Компьютер сегодня в хорошем настроении, поэтому непрочь немного постараться чтобы показать... Сразу 10 новостей!!!")
counter = 0
for i in range(int(u)):
    counter += 1
    print(counter, " |", payload['data']['user']['result']['timeline_v2']['timeline']['instructions'][2]['entries'][i]['content']['itemContent']['tweet_results']['result']['legacy']['full_text'].replace("\n", " ").replace("\r", ""))

